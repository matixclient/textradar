package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.ChatColor;
import de.paxii.clarinet.util.objects.IntObject;

import net.minecraft.entity.player.EntityPlayer;

import java.util.List;

/**
 * Created by Lars on 07.06.2016.
 */
public class ModuleTextRadar extends Module {
  public ModuleTextRadar() {
    super("TextRadar", ModuleCategory.RENDER);

    this.setVersion("1.0.1");
    this.setBuildVersion(16500);
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent event) {
    List<EntityPlayer> playerList = Wrapper.getWorld().playerEntities;
    IntObject yIndex = new IntObject(2);

    playerList.sort((entityPlayer, otherEntityPlayer) -> {
      Float distanceToEntity = Wrapper.getPlayer().getDistanceToEntity(entityPlayer);
      float distanceToOtherEntity = Wrapper.getPlayer().getDistanceToEntity(otherEntityPlayer);

      return distanceToEntity.compareTo(distanceToOtherEntity);
    });

    playerList.forEach((entityPlayer -> {
      if (entityPlayer.getName().equals(Wrapper.getPlayer().getName())) {
        return;
      }

      int distance = (int) Wrapper.getPlayer().getDistanceToEntity(entityPlayer);

      Wrapper.getFontRenderer().drawString(
              ChatColor.stripColor(entityPlayer.getName()) + "[" + distance + "]",
              2,
              yIndex.getInteger(),
              distance >= 10 ? distance >= 30 ? 0x00FF00 : 0xFFAE00 : 0xFF0000
      );
      yIndex.add(10);
    }));
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
